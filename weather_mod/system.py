import requests
from datetime import datetime, date, time

class WeatherForecast:

    def __init__(self, key_api):
        self.key_api = key_api
        self.date = {}
        self.rain_data = {}
        self.rain_file = f'rain estimate.txt'
        self.file_path = '/Users/jaro/Python/weather_api/'

    def get_data(self):
        url = "https://weatherapi-com.p.rapidapi.com/history.json"
        querystring = {"q":"Wroclaw","dt": self.date,"lang":"en"}
        headers = {
            'x-rapidapi-host': "weatherapi-com.p.rapidapi.com",
            'x-rapidapi-key': self.key_api
        }
        response = requests.request("GET", url, headers=headers, params=querystring)
        content = response.json()
        self.data = content
        return content
        
    def get_rain_info(self):
        avg_rain = float(self.data['forecast']['forecastday'][0]['day']['totalprecip_mm'])
        return self.check_rain_data(avg_rain)

    def check_rain_data(self, avg_rain):
        if avg_rain == 0.0:
            return "Nie bedzie padac"
        elif avg_rain > 0.0:
            return "Bedzie padac"
        return "Nie wiem"

    def save_file(self):
        with open (self.rain_file, 'a') as file:
            rain_data = (str(self.date) + str(': ') + str(self.get_rain_info()) + str('\n'))
            file.write(rain_data)
            print(rain_data)

    def check_file(self):
        data_exist = False
        with open(self.rain_file) as file:
            for line in file.readlines():
                check_date = line.split()[0].replace(':', '')
                if check_date == self.date:
                    print(line)
                    data_exist = True
            if not data_exist:    
                self.get_data()
                self.save_file()
                self.rain_data[self.date] = self.get_rain_info()

    def add_to_dict(self):
        with open(self.rain_file) as file:
            for line in file:
                (key, value) = line.split(': ')
                self.rain_data[key] = value
    
    def __getitem__ (self, index):
        self.date = index
        self.check_file()

    def item_gen(self):
        for key, value in self.rain_data.items():
            yield (key, value)

    def items(self):
        self.add_to_dict()
        for i in self.item_gen():
            print(i)

    def __iter__(self):
        self.add_to_dict()
        for date in self.rain_data.keys():
            yield date